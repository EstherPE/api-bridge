const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ROLES = ["player", "admin"];

const role = {
    name: {type: String, enum: ROLES},
};

const roleSchema = new Schema(role, { timestamps: true });

const Role = mongoose.model('Role', roleSchema);
module.exports = Role;