const express = require('express')
const RoleService= require('./roles.service')
const RoleController = express.Router();

RoleController.get('/', async (req, res, next) => {
    try {
        const role = await RoleService.find();
        res.json(role);
    } catch (error) {
        next(error)
    }
});

RoleController.get('/:id', async (req, res, next) => {
    try {
        const { id } = req.params;

        const role = await RoleService.findOne(id);

        res.json(role);
    } catch (error) {
        next(error);
    }
});

RoleController.post('/', async (req, res, next) => {
    try {
        const { name} = req.body;

        const created = await RoleService.create({name});

        res.status(201).json(created);
    } catch (error) {
        next(error);
    }
})

RoleController.put('/:id', async (req, res, next) => {
    try {
        const { name} = req.body;
        const { id } = req.params;

        const updated = await RoleService.replace(id, {name });

        res.json(updated);
    } catch (error) {
        next(error);
    }
})

RoleController.delete('/:id', async (req, res, next) => {
    try {
        const { id } = req.params;

        await RoleService.delete(id);

        res.status(204).send();
    } catch (error) {
        next(error);
    }
})

module.exports = RoleController;