const Role = require('./roles.model')

class RoleService {

    static find() {
        const find = Role.find();
        return  find;
    }

    static async findOne(id) {
        const role = await Role.findById(id);

        if (role) {
            return role;
        }

        throw new StatusError(404, `Role with id <${id}> was not found`);
    }

    static async create(role) {

        // await CustomerResolver.existsById(role.owner);

        return Role.create(role);
    }

    static async replace(id, role) {
        const updated = await Role.findByIdAndUpdate(id, role);

        if (updated) {
            return updated;
        }

        throw new StatusError(404, `Role with id <${id}> was not found`);
    }

    static async delete(id) {
        const role = await Role.findById(id);

        if (role) {
            return Role.findByIdAndRemove(id);
        }

        throw new StatusError(404, `Role with id <${id}> was not found`);
    }
}

module.exports = RoleService;