
const mongoose= require('mongoose');
const Game = require('./game.model')

require('dotenv').config();

const games= [ 
    {
        event:"BBO1-2019WBTC",
        west: "NAB",
        north:"BURAS",
        east:"B DRIJVER",
        south: "NARKIEWICZ",
        board: "61852f417545f3588c70558d",
        // result: "618168ae7f4f42942f1ce98d",
        declarer:"N",
        contract:"4SX",
        result:10,
        score:"NS 590"
    },
    {
        event:"BBO1-2019WBTC",
        west: "NAB",
        north:"BURAS",
        east:"B DRIJVER",
        south: "NARKIEWICZ",
        board: "61852f417545f3588c70558e",
        // result:  "618168ae7f4f42942f1ce98e"
        declarer:"W",
        contract:"3DX",
        result:9,
        score:"NS -470"
    },
    {
        event:"BBO1-2019WBTC",
        west: "NAB",
        north:"BURAS",
        east:"B DRIJVER",
        south: "NARKIEWICZ",
        board: "61852f417545f3588c70558f",
        // result:  "618168ae7f4f42942f1ce98f"
        declarer:"S",
        contract:"2D",
        result:8,
        score:"NS 90"
    },
    {
        event:"BBO1-2019WBTC",
        west: "NAB",
        north:"BURAS",
        east:"B DRIJVER",
        south: "NARKIEWICZ",
        board:  "61852f417545f3588c705590",
        // result: "618168ae7f4f42942f1ce990",
        declarer:"N",
        contract:"4S",
        result:9,
        score:"NS -100"
    },
    {
        event:"BBO1-2019WBTC",
        west: "NAB",
        north:"BURAS",
        east:"B DRIJVER",
        south: "NARKIEWICZ",
        board:  "61852f417545f3588c705591",
        // result: "618168ae7f4f42942f1ce991",
        declarer:"S",
        contract:"3NT",
        result: 6,
        score:"NS -300"
    },
    {
        event:"BBO1-2019WBTC",
        west: "NAB",
        north:"BURAS",
        east:"B DRIJVER",
        south: "NARKIEWICZ",
        board: "61852f417545f3588c705592",
        // result:"618168ae7f4f42942f1ce992",
        declarer:"N",
        contract:"2S",
        result:7,
        score:"NS -50"
    },

]

mongoose
.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
.then(async () => {
      
  const allGame = await Game.find();
      
  if (allGame.length) {
    await Game.collection.drop(); 
  }
})
.catch((err) => console.log(`Error deleting data: ${err}`))
.then(async () => {
      // Una vez vaciada la db de las pelis, usaremos el array movies
      // para llenar nuestra base de datos con todas las pelis.
      await Game.insertMany(games);
  })
.catch((err) => console.log(`Error creating data: ${err}`))
  // Por último nos desconectaremos de la DB.
.finally(() => mongoose.disconnect());