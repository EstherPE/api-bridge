
const PlayerService = require("../../players/player.service");
const StatusError = require("../error/status.error");
const JwtUtils = require("../utils/jwt.utils");

const securedPlayer = async (req, res, next) => {
    try {
        const token = req.headers.authorization;

        if (!token) {
            throw new StatusError(403, "Unauthorized");
        }

        const parsedToken = token.replace('Bearer ', '');

        const validToken = JwtUtils.verifyPlayer(parsedToken);

        const player = await PlayerService.findOne(validToken.id);

        req.user = player;

        next();
    }
    catch (error) {
        next(error);
    }
};

module.exports = securedPlayer;