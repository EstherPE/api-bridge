const Admin = require("../../admin/admin.model");
const Player = require("../../players/player.model")
const Role = require("../../roles/roles.model")
const AdminService = require("../../admin/admin.service");
const StatusError = require("../error/status.error");
const JwtUtils = require("../utils/jwt.utils");

const secured = async (req, res, next) => {
    try {
        const token = req.headers.authorization;

        if (!token) {
            throw new StatusError(403, "Unauthorized");
        }

        const parsedToken = token.replace('Bearer ', '');

        const validToken = JwtUtils.verify(parsedToken);

        const admin = await AdminService.findOne(validToken.id);

        req.user = admin;

        next();
    }
    catch (error) {
        next(error);
    }
};

// const isPlayer = async (req, res, next) => {
//     try {
//       const user = await Player.findById(req.userId);
//       const roles = await Role.find({ _id: { $in: user.roles } });
  
//       for (let i = 0; i < roles.length; i++) {
//         if (roles[i].name === "player") {
//           next();
//           return;
//         }
//       }
  
//       return res.status(403).json({ message: "Require Player Role!" });
//     } catch (error) {
//       console.log(error)
//       return res.status(500).send({ message: error });
//     }
//   };

// const isAdmin = async (req, res, next) => {
// try {
//     const user = await Admin.findById(req.userId);
//     const roles = await Role.find({ _id: { $in: user.roles } });

//     for (let i = 0; i < roles.length; i++) {
//     if (roles[i].name === "admin") {
//         next();
//         return;
//     }
//     }

//     return res.status(403).json({ message: "Require Admin Role!" });
// } catch (error) {
//     console.log(error)
//     return res.status(500).send({ message: error });
// }
// };

module.exports = secured;