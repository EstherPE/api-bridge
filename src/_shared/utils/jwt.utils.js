const jwt = require("jsonwebtoken");

class JwtUtils {
    static generate(id, email) {
        return jwt.sign({ id, email }, process.env.JWT_SECRET, { expiresIn: '1d' });
    }

    static generatePlayer(id, nickname) {
        return jwt.sign({ id, nickname }, process.env.JWT_SECRET_PLAYER, { expiresIn: '1d' });
    }

    static verify(token) {
        return jwt.verify(token, process.env.JWT_SECRET);
    }

    static verifyPlayer(token){
        return jwt.verify(token, process.env.JWT_SECRET_PLAYER);
    }
}

module.exports = JwtUtils;