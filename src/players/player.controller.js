const express = require('express'); 
const PlayerService = require('./player.service');
const PlayerController = express.Router();

PlayerController.get('/', async (req, res, next) => {
    try {
        const player = await PlayerService.find();
        res.json(player);
    } catch (error) {
        next(error)
    }
});

PlayerController.get('/:id', async (req, res, next) => {
    try {
        const { id } = req.params;

        const player = await PlayerService.findOne(id);

        res.json(player);
    } catch (error) {
        next(error);
    }
});

PlayerController.post('/', async (req, res, next) => {
    try {
        const {name, nickname, password } = req.body;

        const created = await PlayerService.create({name, nickname, password });

        res.status(201).json(created);
    } catch (error) {
        next(error);
    }
})

PlayerController.post('/login', async (req, res, next) => {
    try {
        const {name, nickname, password } = req.body;

        const created = await PlayerService.login({ name, nickname, password });
        //imprimimos el token
        res.status(201).json(created);
    } catch (error) {
        next(error);
    }
})



PlayerController.delete('/:id', async (req, res, next) => {
    try {
        const { id } = req.params;

        await PlayerService.delete(id);

        res.status(204).send();
    } catch (error) {
        next(error);
    }
})

module.exports = PlayerController;