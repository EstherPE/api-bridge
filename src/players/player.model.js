const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const player = {
    name: {type:String, required: true},
    nickname: {type: String, required: true},
    password: {type: String, required: true}
}

const playerSchema = new Schema( player, {timestamps:true});

const Player = mongoose.model('Player', playerSchema);
module.exports= Player; 