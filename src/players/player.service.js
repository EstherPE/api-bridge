const StatusError = require("../_shared/error/status.error");
const Player = require("./player.model");
const bcrypt = require('bcrypt');
const JwtUtils = require("../_shared/utils/jwt.utils");

class PlayerService {

    static find() {
        return Player.find();
    }

    static async findOne(id) {
        const player = await Player.findById(id).lean();

        if (player) {
            return player;
        }

        throw new StatusError(404, `Player with id <${id}> was not found`);
    }

    static async create(player) {

        const found = await Player.findOne({nickname: player.nickname });

        if (found) {
            throw new StatusError(400, `Player with nickname ${player.nickname} already exists`);
        }

        const hashedPassword = await bcrypt.hash(player.password, 10);

        return Player.create({ ...player, password: hashedPassword });
    }

    static async login(player) {
        //Encontrar el email del player
        const found = await Player.findOne({ nickname: player.nickname });

        if (!found) {
            throw new StatusError(404, "Player not exists by nickname");
        }
        //Comprobar la contraseña
        const isValidPassword = await bcrypt.compare(player.password, found.password);

        if (!isValidPassword) {
            throw new StatusError(403, "Invalid credentials");
        }
        //General el token
        const token = JwtUtils.generatePlayer(found._id, found.nickname);

        return { token };
    }


    static async delete(id) {
        const player = await Player.findById(id);

        if (player) {
            return Player.findByIdAndRemove(id);
        }

        throw new StatusError(404, `Player with id <${id}> was not found`);
    }
}

module.exports = PlayerService;